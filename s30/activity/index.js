async function fruitsOnSale(db) {
	return await(

			
			db.fruits.aggregate([
				{ $match : {onSale: true}},
				{ $group : {
					_id : null,
					total: {
					$sum: 1
			}}},
			{ $project: { _id: 0 } }
			])

		);
};



async function fruitsInStock(db) {
	return await(

			
			db.fruits.aggregate([
				{ $match : {onSale: true, stock : {$gte : 20}}},
				{ $group : {
					_id : null,
					total: {
					$sum: 1
			}}},
			{ $project: { _id: 0 } }
			])

		);
};



async function fruitsAvePrice(db) {
	return await(

			
			db.fruits.aggregate(
			   [
			   { $match : {onSale: true}},
			     {$group:
			         {
			           _id: "$supplier_id",
			           avgPrice: { $avg: { $sum : "$price"}}
			         }
			     }
			   ]
			)

		);
};



async function fruitsHighPrice(db) {
	return await(

			
			db.fruits.aggregate([
				{ $group: {_id : "$supplier_id", 
					max : {$max : "$stock"}}
				},
					{
						$sort : { _id : 1 }
					}
				])
			

		);
};




async function fruitsLowPrice(db) {
	return await(

			// Add query here
			db.fruits.aggregate([
				{ $group: {_id : "$supplier_id", 
					min : {$min : "$stock"}}
				},
					{
						$sort : { _id : 1 }
					}
				])

		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
