// console.log("Hello")


let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999,
}

console.log('REsult from creating objects using initializer/literal notation: ')

let exampleArray = [1, 2, 3, 4, 5]

console.log(exampleArray);
console.log(typeof exampleArray);


console.log(cellphone);
console.log(typeof cellphone);



function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}


let laptop = new Laptop('Lenovo', 2008);
console.log('Result from creating using object contructors: ')
console.log(laptop);


let myLaptop = new Laptop('Macbook Aair', 2020)
console.log('Result from creating using object contructors: ')
console.log(myLaptop);


let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log('Result from creating using object contructors: ')
console.log(oldLaptop);


let computer = {}
let myComputer = new Object();

console.log('Result from dot notation: ' + myLaptop.name);

console.log('Result from square notation: ' + myLaptop['name']);


let array = [laptop, myLaptop]

console.log(array[0]['name']);
console.log(array[0].name);


let car = {}

car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation');
console.log(car);


car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log('Result from adding properties using square bracket notation');
console.log(car);

delete car['manufacture date'];
console.log('Result from deleting properties');
console.log(car);

/*car.name = 'Dodge Charger R/T'
console.log('Result from deleting properties');.
console.log(car);
*/

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name);
	}
}

console.log(person);
console.log('Result from object methods: ');
person.talk()


person.walk = function(){
	console.log(this.name + ' walked 25 steps forward')
};
person.walk()

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		state: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@mail.xyz'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName)
	}
};
friend.introduce();
console.log(friend)


let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint : function(){
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);


function Pokemon (name, level){

	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level

	this.tackle = function(target){
		console.log(this.name + 'tackled ' + target.name);
		console.log("targetPokemn's health is now reduced to_targetPokemonhealth_");
	};
	this.faint = function(){
		console.log(this.name + 'fainted');
	}
}


let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);


pikachu.tackle(rattata);






















