// Advanced Queries

// Query an embedded document / object
db.users.find({
	contact : {
		phone: "87654321",
		email : "stephenhawking@gmail.com"
	}
})

// Dot Notation
db.users.find({
	"contact.email" : "stephenhawking@gmail.com"
})


// querying an array with exact element
db.users.find({
	courses: ["CSS", "Javascript", "Python"]
});

db.users.find({courses: {
	$all: ["React", "Laravel"]
}})

// Query Operators

//  [Section] Comparison Query Operator
	// $gt/$gte operator
		/*
			-allows us to find documents that have field number values greater or equal to a specified value.
			-note that this operator will only work if the data type of the field is number or integer.
			Syntax:
			db.collectionName.find({ field: {
				$gt: value
			}})
			db.collectionName.find({ field: {
				$gte: value
			}})

		*/

db.users.find({age : { $gt: 76}});

db.users.find({age : { $gte: 76}});
	// $lt/$lte operator
		/*
			-allows us to find documents that have field number values less than or equal to a specified value
			-note: same with $gt/gte operator, this will only work if the data type of the field being queried is number or Int.
			Syntax:
			db.collectionName.find({field: {$lt : value}});
			db.collectionName.find({field: {$lte : value}});
		*/

db.users.find({age : { $lt : 76}});

db.users.find({age : { $lte : 76}});

	// $ne operator
	/*
		-allows us to find documents that have field number values not equal to specified value
		Syntax:
		db.collectionName.find({field: {$ne : value}});
	*/

db.users.find({age: {$ne: 76}});


	// $in operator
	/*
		-allows us to find documents that match a single criteria from multiple provided search criteria
		Syntax:
		db.collectionName.find({
			field :
				{
					$in: value
				}
		})
	*/

db.users.find({lastName: { $in : ["Hawking", "Doe", "Armstrong"]}});

db.users.find({"contact.phone" : { $in : ["87654321"]}});

// We can use this in an array
db.users.find({courses: {$in : ["React"]}});

db.users.find({courses: {$in : ["React", "Laravel"]}});

db.users.find({lastName: "Hawking"});
db.users.find({lastName: "Doe"});

// [Section] Logical Query Operators
	//  $or operator
	/*
		- allows us to find documents that match a single criteria from multiple provided search criteria.
		Syntax:
			db.collectionName.find({
				$or : [{fieldA: value}, {fieldB: valueB}]
			});

	*/

db.users.find({$or : [{firstName: "Neil"}, {age: 25}]});


// add multiple operators
db.users.find({
	$or : 
	[
		{firstName: "Neil"}, 
		{age: {$gt:25}}, 
		{courses: {$in : ["HTML"]}}
	]
});


	// $and operator
	/*
		-allows us to find documents matching all the multiple criteria in a single field.
		Syntax:
		db.collectionName.find({
			$and:
			[
				{fieldA: valueA},
				{fieldB: valueB}
				. . .
			]
		})
	*/


db.users.find({
	$and:
	[
		{age : {$ne: 82}},
		{age : {$ne: 76}}
	]
});

// Mini-Activity

db.users.find({
	$and : 
	[
		{age: {$lt: 30}}, 
		{courses: {$in : ["HTML", "CSS"]}}
	]
});

// [Section] Field Projection
	/*
		-retrieving documents are common operations that we do and by default mongoDB queries return the whole document as a response.
	*/

	// Inclusion
	/*
		-allows us to include or add specific fields only when retrieving documents:

		Syntax:
		db.collectionName.find({criteria}, {field: 1});
	*/

db.users.find(
	{firstName : "Jane"},
	{
		firstName : 1,
		lastName : 1,
		contact: 1,
		_id: 0
	}

	)


db.users.find(
	{firstName : "Jane"},
	{
		firstName : 1,
		lastName : 1,
		"contact.phone": 1,
		_id: 0
	}

	)


	// Exclusion
	/*
		allows us to exclude/remove specific fields only when retrieving documents
		Syntax:
			db.collectionName({criteria}, {field : 0})
	*/

db.users.find(
	{firstName : "Jane"},
	{
		contact: 0,
		department: 0
	}
	)

db.users.find(
	{firstName : "Jane"},
	{
		"contact.email": 0,
		department: 0
	}
	)


db.users.find(
	{$or:
		[{firstName: "Jane"},
			{age : {$gte : 30}}
			]
		},

	{
		"contact.email" : 0,
		department: 0
	}
	)

db.users.find(
	{$or:
		[{firstName: "Jane"},
			{age : {$gte : 30}}
			]
		},
	{
		"contact.email" : 0,
		department: 0,
		courses: {$slice : 1}
	}
	)

// Evaluation Query Operator
	// $regex operator
		/*
			-allows us to find documents that match a specific string pattern using regular expressions.
			Syntax:
			db.users.find({
					field: $regex: 'pattern', option:
					'optionValue'
			})
		*/

// Case sensitive
db.users.find({firstName : { $regex: 'N'}});

// case insensitive
db.users.find({firstName : { $regex: 'n', $options: 'i'}});








