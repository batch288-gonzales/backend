const express = require("express");

const taskControllers = require('../controllers/taskControllers.js');

// Contain all the endpoints of all our application
const router = express.Router();

router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks)

// Parameterized

// We are create a oute using a Delete method at the URL "/tasks/:id"
// The colon here us an identifier that helps create a dinamic route which allows us to supply information
router.delete("/:id", taskControllers.deleteTask);




/*-------------------*/

// Activity

router.get("/:id", taskControllers.getSpecificTask);

router.put("/:id/complete", taskControllers.putSpecificTask);


module.exports = router;