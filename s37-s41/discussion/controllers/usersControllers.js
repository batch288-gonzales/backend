const Users = require("../models/Users.js");
const Courses = require("../models/Courses.js");

const bcrypt =require("bcrypt");

//Require the auth.js
const auth = require('../auth.js');

// Controllers

//Create a controller for the signup
//registerUser
/* Business Logic/ Flow */
 	// 1. First, we have to validate whether the user is existing or not. We can do that by valiadting whether the email exist on our databases or not.
	//2. If the user email is existing, we will prompt an error telling the user that the email is taken.
	//3. otherwise, we will sign up or add the user in our database.
module.exports.registerUser = (request, response) => {
	
	
	//find method: it will return the an array of object that fits the given criteria

	Users.findOne({email : request.body.email})
	.then(result => {

		// we need add if statement to verify whether the email exist already in our database
		if(result){

			return response.json(false)
		}else{
			if (request.body.password1 !== request.body.password2) {
          	return response.json(false);
        }

			//Create a new Object instantiated using the Users Model.
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				//hashSync method : it hash/encrypt our password
				//the second argument salt rounds
				password: bcrypt.hashSync(request.body.password1, 10),
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo
			})


			// Save the user
			//Error handling
			newUser.save()
			.then(saved => response.json(true))
			.catch(error => response.json(false));



		}

	})
	.catch(error => response.json(false));

}

//new controller for the authentication of the user
module.exports.loginUser = (request, response) => {

	Users.findOne({email : request.body.email})
	.then(result => {

		if(!result){
			return response.send(false)
		}else{
			//the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve form the find method. returns true or false depending the result of the comparison.
			let isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send({
					auth : auth.createAccessToken(result)
				}
				)
			}else{
				return response.send(false)
			}


		}

	})
	.catch(error => response.send(false));

}

module.exports.getProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	// console.log(userData)

	if(userData.isAdmin){
		Users.findOne({_id : request.body.id})
	.then(result => {
		
		password : result.password = ""

		return response.send(result)
	})
	.catch(error => response.send(false));

	}else{
		return response.send(false)
	}

	
}

// Controller for the enroll course
module.exports.enrollCourse = (request, response) => {

	const courseId = request.body.id;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false)
	}else{

		// Push papunta kay user document
	let isUserUpdated = Users.findOne({_id : userData.id})
		.then(result => {
			result.enrollments.push({
				courseId : courseId
			})

			result.save()
			.then(saved = true)
			.catch(error => false)

		})
		.catch(error => false)

		/*let totalAmount = {};
		Users.forEach( arrayOfObjects => {
		  arrayOfObjects.forEach( obj => {
		    if(totalAmount[obj._id] != null) {
		      totalAmount[obj._id] += obj.total
		    }else{
		      totalAmount[obj._id] = obj.total
		    }
		  })
		})
		let result = [];
		for ( [key,value] of Object.entries(temp) ){
		  result.push({ _id: key*1, total: value })
		}*/




		// Push papunta kay course document

		let isCourseUpdated = Courses.findOne({_id : courseId})
			.then(result => {
				result.enrollees.push({userId : userData.id})
			

			result.save()
			.then(saved => true)
			.catch(error => false)

			})
			.catch(error => false)

			// if(isUserUpdated && totalAmount && isCourseUpdated)
			if(isUserUpdated && isCourseUpdated){
				return response.send(true)
			}else{
				return response.send(false)
			}

	}

}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id : userData.id})
	.then(data => response.send(data))
}
