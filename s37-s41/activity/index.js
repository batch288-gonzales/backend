const express = require('express');

const mongoose = require('mongoose');

// It will allow our backedn application to be available to our frontend application
// It will also allows us to control app's Cross Origin resource Sharing settings

const cors = require('cors')

const usersRoutes = require('./routes/usersRoutes.js')

const coursesRoutes = require('./routes/coursesRoutes.js')

const port = 4001;

const app = express();

    // Mongo DB connection
    // Extablish the connection between the db and the applicattion or server.
    // The name of the database should CourseBookingAPI

 	mongoose.connect("mongodb+srv://admin:admin@batch288gonzales.zfsmxfa.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

 	let db = mongoose.connection;

 	db.on("error", console.error.bind(console, "Error! can't connect to the database"));

 	db.once("open", () => console.log("We're connected to the cloud database!"));


 	// Middlewares
 	app.use(express.json())
 	app.use(express.urlencoded({extended:true}))

    // Remider that we are going to use this for the sake of the bootcamp
    app.use(cors());

    // add routing of the routes from the usersRoutes
    app.use('/users', usersRoutes)
    app.use('/courses', coursesRoutes)




    if(require.main === module){
        app.listen(process.env.PORT || 4000, () => {
            console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
        });
    }